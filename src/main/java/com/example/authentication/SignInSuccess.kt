package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_sign_in_success.*

class SignInSuccess : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in_success)
        backToMain()
    }
    private fun backToMain(){
        back.setOnClickListener {
            val intent = Intent(this,Authentication::class.java)
            startActivity(intent)
        }
    }

}
